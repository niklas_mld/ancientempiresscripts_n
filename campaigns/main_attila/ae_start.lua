-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--
--    CAMPAIGN SCRIPT
--
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

intro_cinematic_str = false;

function set_intro_cinematic(str)
    intro_cinematic_str = str;
end;

-- play intro movie
cm:register_ui_created_callback(
    function()
        if cm:is_new_game() then
            
            -- if one of the faction scripts has set an intro movie string, play it
            if is_string(intro_cinematic_str) then
                --output("Playing intro movie, key is [" .. intro_cinematic_str .. "]");
                --cm:register_movies(intro_cinematic_str);
            end;
        end;
    end
);


-- camera extents
--cm:set_default_zoom_limit(1.00, 0.8);

function init_hannibal_attack()
    local scipio_char_cqi = 8
    local scipio_char = get_character_from_faction_by_cqi(get_faction("rom_rome"), scipio_char_cqi)
    local scipio_char_str = char_lookup_str(scipio_char)
    
    -- Scipio can't move during the first turn
    cm:zero_action_points(scipio_char_str)

    local hannibal_char_cqi = 70
    local hannibal_char = get_character_from_faction_by_cqi(get_faction("rom_carthage"), hannibal_char_cqi)
    local hannibal_char_str = char_lookup_str(hannibal_char)

    -- Zero hannibal points too so AI doesn't make any plans
    cm:zero_action_points(hannibal_char_str)

    cm:add_turn_start_callback_for_faction(
        "CarthageTurnListener", 
        "rom_carthage", 
        function()
            --[[cm:add_listener(
                "pre_battle_ui_listener",
                "PanelOpenedCampaign",
                true,
                preBattleScreen,
                false]]
            --output("speedup active?: " .. cm:speedup_active())
            sendHannibalToAttackScipio()
        end,
        false
    )
end

-- put stuff that needs to be started regardless of the faction being played here
function start_game_all_factions()
    output("start_game_all_factions() called")
    
    output("debug disabling show shroud!")
    cm:show_shroud(false)

    init_hannibal_attack()
    
    --[[
    if cm:is_multiplayer() == false then
        -----------------------------
        ------- SINGLE PLAYER -------
        -----------------------------

    else
        -----------------------------
        ------- MULTI PLAYER --------
        -----------------------------
    
    end]]
end;
