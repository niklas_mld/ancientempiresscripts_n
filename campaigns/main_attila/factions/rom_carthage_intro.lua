-- this line needs to be present - due to the way this script file is loaded by the main script it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.script_env);

-------------------------------------------------------
--    Intro cutscene construction
--    This function declares and configures the cutscene 
--    and loads it with actions.
--    Customise it to suit.
-------------------------------------------------------

bool_intro_cutscene_advice_played = false;

function play_intro_cutscene_advice()
    if bool_intro_cutscene_advice_played then
        return;
    end;
    
    bool_intro_cutscene_advice_played = true;
    
    --cm:show_advice("att_advice_intro_flyby_roman_west", true)    --Be replaced with "ae_advice_intro_flyby_roman_republic"
end;

function cutscene_intro_construct()
    cutscene_intro = campaign_cutscene:new(
        local_faction .. "_intro",		-- string name for this cutscene
        16,                             -- length of cutscene in seconds
        function() start_faction() end  -- end callback
    );
    
    --cutscene_intro:set_debug();
    ui_state.toggle_movement_speed:lock()

    cutscene_intro:set_skippable(true, sendHannibalToAttackScipio) -- If the cutscene is skipped, make sure that hannibal still moves to fight scipio

    cutscene_intro:set_disable_settlement_labels(true)
    
    --cutscene_intro:set_skip_camera(cam_start_x, cam_start_y, cam_start_h, cam_start_r);

    cutscene_intro:action(
        function() 
            output("zooming into hannibal")
        
            cm:scroll_camera_with_direction(
                6, 
                {cam_start_x, cam_start_y, 0.7, cam_start_r}, -- Hannibal
                {cam_start_x, cam_start_y, 1.0, cam_start_r}
            ) 
        end, 
        0
    )
    
    cutscene_intro:action(
        function() 
            output("panning to scipio")
            sendHannibalToAttackScipio()
        
            cm:scroll_camera_with_direction(
                10,                                     
                {cam_start_x, cam_start_y, 1.0, cam_start_r}, 
                {173.8, 201.5, 1.0, cam_start_r} -- Scipio 
            ) 
        end, 
        6     --Time of the camera action in seconds
    )
end


-------------------------------------------------------
--    Construct and then start the cutscene
-------------------------------------------------------

function cutscene_intro_play()
    cm:show_shroud(false)
    
    cm:add_listener(
        "pre_battle_ui_listener",
        "PanelOpenedCampaign",
        true,
        preBattleScreen,
        false
    )
    
    --cm:lock_ui()
    cutscene_intro_construct()
    cutscene_intro:start()
end
