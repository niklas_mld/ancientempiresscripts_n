
-- this line needs to be present - due to the way this script file is loaded by the main script it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.script_env);


-------------------------------------------------------
--	Intro cutscene construction
--	This function declares and configures the cutscene 
--	and loads it with actions.
--	Customise it to suit.
-------------------------------------------------------

bool_intro_cutscene_advice_played = false;

function play_intro_cutscene_advice()
	if bool_intro_cutscene_advice_played then
		return;
	end;
	
	bool_intro_cutscene_advice_played = true;
	
	cm:show_advice("ae_advice_intro_flyby_seleucid", true)	
end;


function cutscene_intro_construct()

	cutscene_intro = campaign_cutscene:new(
		local_faction .. "_intro",							-- string name for this cutscene
		85,													-- length of cutscene in seconds
		function() start_faction() end						-- end callback
	);

	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_seleucid", "att_reg_khwarasan_merv") end, 0);


	--cutscene_intro:set_debug();
	cutscene_intro:set_skippable(true, function() cutscene_intro_skipped() end);
	cutscene_intro:set_disable_settlement_labels(false);
	
	
	cutscene_intro:action(function() play_intro_cutscene_advice() end, 1.5);	
	
	cutscene_intro:set_skip_camera(cam_start_x, cam_start_y, cam_start_h, cam_start_r);

	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
				5, 
				{414.2, 224.6, 1.3, cam_start_r}, 
				{414.2, 224.6, 0.7, cam_start_r}
			) 
		end, 
		0
	);
	
	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
			                27, 									-- Speed of camera 
				{414.2, 224.6, 0.7, cam_start_r}, --Antioch
				{490, 194, 0.5, cam_start_r},		--ctesiophon
				{516, 163, 0.5, 0.5},			--meshan	
				{574, 136, 0.5, cam_start_r},		--harmosia
				{568, 187, 0.5, cam_start_r},		--issatis
				{591, 225, 1, 0.7}		--khwarasan_abarshahr	

			) 
		end, 
		7 	--Time of the camera action in seconds
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				4,
				{591, 225, 1, 0.7},		--khwarasan_abarshahr
				{610.3, 213.4, 1, -0.3}	--harey
						
			)
		end,
		37

	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				4,
				{610.3, 213.4, 1, -0.3},		--harey
				{608.3, 245.5, 1, -0.7}	--merv
						
			)
		end,
		42

	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				14,
				{608.3, 245.5, 1, -0.7},		--merv
				{405.1, 172.1, 0.7, 1.2}	--Jeru
				
			) 
		end, 
		48
	);
	
	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
				20, 
				{405.1, 172.1, 0.7, 1.2},  --Jeru
				{362.3, 230, 0.5, 1},  --Asia minor
				{366.3, 260.9, 0.5, .8},  --Asia minor
				{384.4, 250.5, 0.7, .7},  --Asia minor
				{414.2, 224.6, 1, cam_start_r}   --Anticoch	
				
			) 
		end, 
		65
	);
end;


function cutscene_intro_skipped()
	play_intro_cutscene_advice()
end;


-------------------------------------------------------
--	Construct and then start the cutscene
-------------------------------------------------------

function cutscene_intro_play()
	cutscene_intro_construct();
	cutscene_intro:start();
end;



