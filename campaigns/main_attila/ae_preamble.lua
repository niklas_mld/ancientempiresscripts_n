--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--
--    PREAMBLE SCRIPT
--    handles details relating to the battle of zama at the start of the campaign
--
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
-- TODO: Make this proper and safe etc with properly enabling/disabling UI elements if battle happens after turn end
-- Otherwise, make battle happen during turn by disabling end of turn warnings?
-------------------------------------------------------------------------------------

function sendHannibalToAttackScipio()
    -- find hannibal
    local hannibal_char_cqi = 70
    local hannibal_char = get_character_from_faction_by_cqi(get_faction("rom_carthage"), hannibal_char_cqi)
    local hannibal_char_str = char_lookup_str(hannibal_char)
    
    -- find scipio
    local scipio_char_cqi = 8
    local scipio_char = get_character_from_faction_by_cqi(get_faction("rom_rome"), scipio_char_cqi)
    local scipio_char_str = char_lookup_str(scipio_char)

    -- We can't force scipio to not retreat, afaik...
    -- disabling movement and zeroing action points doesn't work.
    
    output("sending hannibal to attack scipio")
    sendToAttack(hannibal_char_str, scipio_char_str)
end

function preBattleScreen()
    local uic_prebattle = find_uicomponent(cm:ui_root(), "popup_pre_battle")
    output("UI panel opened")
    if uic_prebattle then
        cm:remove_listener("pre_battle_ui_listener")
        cm:unlock_ui()
        output("hiding retreat button...")
        ui_state.retreat:set_allowed(false, true)
    end
end

function moveTo(char_str, log_x, log_y)    
    cm:enable_movement_for_character(char_str)
    cm:force_character_force_into_stance(char_str, "MILITARY_FORCE_ACTIVE_STANCE_TYPE_DEFAULT")
    cm:replenish_action_points(char_str)

    cm:move_to(char_str, log_x, log_y, true)
    
    -- listen for the army running out of movement points
    cm:add_listener(
        "movement_points_exhausted_" .. char_str,
        "MovementPointsExhausted",
        true,
        function()
            output("MovementPointsExhausted event has happened for " .. char_str)
            moveTo(char_str, log_x, log_y)
        end,
        true
    );

end

function finishedAttack(attacker_str)
    cm:remove_listener("move_attack_finished_" .. attacker_str)
    cm:remove_listener("movement_points_exhausted_" .. attacker_str)
end

function sendToAttack(attacker_str, target_str)
    -- do the attack
    cm:enable_movement_for_character(attacker_str)
    cm:force_character_force_into_stance(attacker_str, "MILITARY_FORCE_ACTIVE_STANCE_TYPE_DEFAULT")
    cm:replenish_action_points(attacker_str)
    
    cm:attack(attacker_str, target_str, true)
    
    cm:add_listener(
        "move_attack_finished_" .. attacker_str,
        "PendingBattle",
        true,
        function() cm:zero_action_points(attacker_str); finishedAttack(attacker_str) end,
        false
    )

    -- listen for movement points running out, if they do, restart this
    cm:add_listener(
        "movement_points_exhausted_" .. attacker_str,
        "MovementPointsExhausted",
        true,
        function()
            output("MovementPointsExhausted event has happened for "  .. attacker_str)
            sendToAttack(attacker_str, target_str)
        end,
        false
    )
end

function activateZamaBattlefield()
    output("adding custom battlefield")

    -- launch Zama battle
    cm:add_custom_battlefield(
        "zama",                                     -- string identifier
        0,                                          -- x co-ord
        0,                                          -- y co-ord
        5000,                                       -- radius around position
        false,                                      -- will campaign be dumped
        "",                                         -- loading override
        "campaigns/main_attila/battle_zama.lua",    -- script override
        "",                                         -- entire battle override
        0,                                          -- human alliance when battle override
        false,                                      -- launch battle immediately
        true                                        -- is land battle (only for launch battle immediately)
    )
end