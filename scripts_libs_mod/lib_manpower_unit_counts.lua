--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- lib_manpower_unit_counts.lua
-- Stores table holding unit sizes

-- This is auto-generated right now. manual changes may be overwritten!

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

units = {
	ai_achaean_argive_cavalry = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_achaean_argive_phalanx = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_achaean_epilektoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_achaean_epilektoi_cavalry = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_achaean_euzonoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_achaean_peltast = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_achaean_peltast_sword = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_achaean_phalanx = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_achaean_slingers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_achaean_tarantine_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_achaean_tarantine_cavalry_late = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_achaean_thorakitai = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_achaean_thorakitai_late = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_achaean_thureophoroi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_achaean_thureophoroi_late = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aethiopian_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aethiopian_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aethiopian_spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aethiopian_warriors = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aetolian_cavalry = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_aetolian_epilektoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aetolian_levies = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_achaean_epilektoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_achaean_peltast = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_aestii_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_aetolian_allied_euzonoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_alpine_spear_levies = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_anatolian_militia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_arab_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_arab_camel_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_arab_camel_spearmen = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_arab_spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aor_armenian_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_armenian_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_armenian_levies = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aor_bactrian_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aor_bactrian_horse_archers = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_aor_bactrian_javelinmen = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_bactrian_militia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_bactrian_pantodapoi = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_bactrian_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_balearic = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_balearic_chieftain = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_balearic_spear = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_bastarnae = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_batavi_swordsmen = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_belgic_cavalry_treverii = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_belgic_infantry = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_bithynian_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aor_bithynian_spearmen = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_blacksea_spr = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_aor_bosp_inf = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_aor_breuci_infantry = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_briton_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_briton_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_cadusii_hillmen = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_aor_cadusii_horse = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_cal_swordsmen = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_campanian_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_aor_cappa_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_cappa_cavalry_light = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_cappa_levies = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aor_cappa_swords = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_carians = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_cauc_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_cauc_levies = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aor_celtic_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_chalybes_pikes = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_chalybes_pikes_2 = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_chatti_infantry = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_cilician_skirm = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aor_cretan_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aor_cretan_euzonoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_cypriot_hoplites = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_cyrtian_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_dacian_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_dacian_infantry = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_dardanian_infantry = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_dii_swordsmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_egyptian_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_egyptian_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_egyptian_machimoi = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aor_epirus_militia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_etruscan_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_aor_etruscan_infantry = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_fenni_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_gaetulian_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_gaetulian_javelinmen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aor_galatian_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_galatian_infantry = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_gallic_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_gallic_infantry = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_german_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_german_east_spear = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_german_east_sword_nobles = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_german_levies = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_german_south_spear = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_german_south_sword_nobles = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_german_west_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_german_west_spear = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_greek_colonists = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_aor_greek_thureophoroi_ambraciot = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_greek_thureophoroi_anatolian = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_greek_thureophoroi_bactria = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_greek_thureophoroi_epirus = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_greek_thureophoroi_macedonian = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_greek_thureophoroi_median = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_greek_thureophoroi_megalopolitan = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_greek_thureophoroi_pergamon = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_greek_thureophoroi_pontic = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_greek_thureophoroi_ptolemaic = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_greek_thureophoroi_syrian = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_harii_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_hel_agranian = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_hel_cretan_archers_allied = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aor_hel_thraikes = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_helveti_spears = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_ibe_cantabrian_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_cantabrian_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_cantabrian_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_cantabrian_cavalry = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_cantabrian_cavalry_noble = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_cantabrian_guerillas = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_cantabrian_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_cantabrian_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_cantabrian_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_cantabrian_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_cavalry_light = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_celtiberian_arevaci_cavalry_noble = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_ibe_celtiberian_arevaci_cavalry_noble_2 = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_ibe_celtiberian_arevaci_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_ibe_celtiberian_arevaci_warrior_spearmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_warrior_spearmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_warrior_spearmen_3 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_warrior_swordsmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_warrior_swordsmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_arevaci_warrior_swordsmen_3 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_cavalry_light = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_celtiberian_cavalry_noble = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_ibe_celtiberian_cavalry_noble_2 = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_ibe_celtiberian_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_warrior_spearmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_warrior_spearmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_warrior_swordsmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_celtiberian_warrior_swordsmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_gallaeci_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_gallaeci_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_gallaeci_cavalry_noble = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_gallaeci_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_gallaeci_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_gallaeci_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_gallaeci_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_gallaeci_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_ibe_gallaeci_tribesmen = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_cavalry_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_iberian_cavalry_scutari = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_iberian_cessetani_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_cessetani_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_cessetani_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_cessetani_cavalry_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_iberian_cessetani_cavalry_scutari = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_ibe_iberian_cessetani_nobles_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_cessetani_nobles_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_cessetani_scutari_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_cessetani_scutari_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_cessetani_scutari_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_cessetani_scutari_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_cavalry_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_iberian_edetani_cavalry_scutari = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_iberian_edetani_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_scutari_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_scutari_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_scutari_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_edetani_scutari_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_scutari_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_scutari_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_scutari_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_scutari_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_cavalry_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_iberian_turdetani_cavalry_scutari = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_ibe_iberian_turdetani_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_scutari_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_scutari_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_scutari_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_iberian_turdetani_scutari_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_caetrati_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_caetrati_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_caetrati_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_caetrati_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_cavalry_noble = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_ibe_lusitani_mountain_tribesmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_mountain_tribesmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_nobles_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_nobles_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_ibe_lusitani_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_ibe_lustiani_cavalry_light = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_illyrian_peltast = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_italian_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_aor_jewish_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_jewish_samaritan_cavalry = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_aor_jewish_samaritan_infantry = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aor_jewish_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_jewish_spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aor_kurdish_spear = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aor_latin_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_aor_latin_infantry = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_libyan_archers_nomad = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_libyan_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_libyan_cavalry_nomad = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_libyan_javelinmen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aor_libyan_javelinmen_nomad = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aor_libyan_slingers_nomad = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_libyan_spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aor_ligurian_infantry = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_lucanian_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_aor_lucanian_spearmen = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_lydian_javelinmen = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_macedonian_militia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_massalian_celtic_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_massalian_celtic_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aor_massalian_celtic_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_massalian_celtic_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_massalian_militia = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_aor_massalian_militia_cavalry = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_aor_mauri_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_mauri_cavalry_light = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_mauri_javelinmen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aor_median_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_median_cavalry_light = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_median_javelinmen = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_median_militia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_median_noble_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_median_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_megalopolitan_cavalry = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_aor_megalopolitan_pikemen = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_mysian_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aor_numidian_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_numidian_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_numidian_javelinmen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aor_numidian_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_paeonian_cavalry = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_pamphylian_peltast = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aor_paphla_cav = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_paphla_inf = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_pergamon_militia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_persian_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_persian_cavalry_light = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_persian_cavalry_noble = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_persian_javelinmen = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_persian_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_pikemen_achaean = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_pikemen_ambraciot = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_pikemen_bactria = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_pikemen_egyptian = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_pikemen_epirus = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_pikemen_macedonian = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_pikemen_pergamon = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_pikemen_pontic = {
		culture = "other",
		class = "poor",
		count = 256
	},

	ai_aor_pikemen_syrian = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_pisidian_hilmen = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_aor_pontic_militia = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aor_ptolemaic_militia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_ptolemaic_militia_cavalry = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_aor_rhodian_slingers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aor_samnite_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_aor_samnite_spearmen = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_sardinian_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_seleucid_militia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_seleucid_militia_cav = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_aor_seleucid_militia_cav_archer = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_aor_sicilian_hoplites = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_sicilian_hoplites_2 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_sicilians = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_aor_steppe_east_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_steppe_east_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_aor_steppe_east_horse_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_steppe_sarm_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_steppe_sarm_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_aor_steppe_sarm_horse_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_steppe_west_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_steppe_west_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_aor_steppe_west_horse_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aor_syrian_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aor_syrian_levies = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_aor_tarantine_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_aor_tarantine_hoplites = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_tarantine_pikemen = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_aor_tencteri_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_thessalian_cavalry_early = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_aor_thessalian_cavalry_late = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_aor_thracian_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aor_thracian_cavalry_heavy = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aor_thracian_cavalry_light = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_thracian_infantry = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_aor_trallian_euzonoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aor_usipii_cavalry = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_aor_venedi_skirms = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_Arab_Camel_Archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Camel_Bowmen = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Camel_Cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Camel_Mounted_Infantry = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Camel_Mounted_Troops = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Camel_Riders = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Camel_Warriors = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Caravan_Guard = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_Arab_Citizen_Milita_Archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Arab_Citizen_Milita_Skirmishers = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Citizen_Militia_Cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Citizen_Militia_Spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Arab_City_State_Archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Arab_City_State_Cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_City_State_Skirmishers = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_City_State_Spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Arab_Clan_Archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Arab_Clan_Champions = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Clan_Light_Horse = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Clan_Raiders = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Arab_Clan_Slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Arab_Clan_Warlord = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Clan_Warriors = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Arab_Frontier_Guard = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Levied_Archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Arab_Levied_Skirmishers = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Levied_Spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Arab_Mounted_Nobles = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_Arab_Nobility = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Noble_Horse = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_Arab_Noble_Horsemen = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_Arab_Noblemen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Nobles = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Royal_Bodyguard = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Royal_Escort = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Royal_Guard = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Swordsmen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Thureophoroi = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Tribal_Archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Arab_Tribal_Champions = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Tribal_Horsemen = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Tribal_Leader = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Tribal_Light_Horse = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Arab_Tribal_Slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Arab_Tribal_Warlord = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Arab_Tribal_Warriors = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_arm_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_arm_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_arm_elite_spears = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_arm_horse_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_arm_less_nobles = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_arm_levy_spears = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_arm_light_inf = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_arm_noble_sword = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_arm_romanised = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_arm_royal_cav = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_arm_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_arm_upper_nobles = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_arqamani = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_attalus = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_augustan_leg_iF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_iiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_iiiaF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_iiiaN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_iiicF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_iiicN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_iiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_iiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_iiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_iN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_ivF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_ivN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_ivsF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_ivsN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_vF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_vicF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_vicN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_viF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_viiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_viiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_viiiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_viiiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_viiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_viiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_viN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_vmF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_vmN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_vN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xfF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xfN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xiiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xiiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xivF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xivN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xixF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xixN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xvF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xviF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xviiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xviiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xviiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xviiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xviN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xvN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xxF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xxiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xxiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_augustan_leg_xxiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xxiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_augustan_leg_xxN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_archer_antioch = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aux_archer_ascalon = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aux_archer_bithynia = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aux_archer_bospori = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aux_archer_canatha = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aux_archer_cav_cappadocia = {
		culture = "barbarian",
		class = "med",
		count = 180
	},

	ai_aux_archer_cav_parthia = {
		culture = "barbarian",
		class = "med",
		count = 180
	},

	ai_aux_archer_cav_syria = {
		culture = "barbarian",
		class = "med",
		count = 180
	},

	ai_aux_archer_cav_thrace = {
		culture = "barbarian",
		class = "med",
		count = 180
	},

	ai_aux_archer_crete = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aux_archer_cyrene = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aux_archer_damascus = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aux_archer_hama = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aux_archer_hama_imp = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aux_archer_italy = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aux_archer_italy_area2 = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aux_archer_italy_area3 = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aux_archer_petra = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_aux_archer_thrace = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_aux_archer_tyre = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_aux_cataphract_1 = {
		culture = "barbarian",
		class = "rich",
		count = 120
	},

	ai_aux_cataphract_2 = {
		culture = "barbarian",
		class = "rich",
		count = 120
	},

	ai_aux_cav_africa = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_aux_cav_asturias = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_batavia = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_bospori = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_aux_cav_britain = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_camel = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_aux_cav_cananefates = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_cappadocia = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_aux_cav_dacia = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_dardani = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_gaetuli = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_aux_cav_gaul = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_iberia = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_nervii = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_noricum = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_numidia = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_aux_cav_pannonia = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_parthia = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_aux_cav_pontus = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_aux_cav_samaria = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_scubuli = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_syria = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_aux_cav_thrace = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_treveri = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_tungri = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_cav_voconti = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_aux_infantry_africa = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_africa_imp = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_alpina = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_alpina_imp = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_aquitania = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_asturias = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_baetica = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_batavia = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_batavia_imp = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_bithynia = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_bituriges = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_bospori = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_bracares = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_breuci = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_britain = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_britain_imp = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_cananefates = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_cantabri = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_cappadocia = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_celtiberian = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_cilicia = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_corsica = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_cugerni = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_cyprus = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_cyrene = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_cyrrhus = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_dacia = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_dacia_imp = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_delmatia = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_delmatia_imp = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_egypt = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_frisii = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_gaetuli = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_galatia = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_gaul = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_gaul_imp = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_helvetii = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_iberia = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_iberia_imp = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_italy = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_italy_area2 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_italy_area3 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_italy_imp = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_italy_imp_area2 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_italy_imp_area3 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_liguria = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_lingones = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_lucensium = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_lusitania = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_macedonia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_aux_infantry_mattiaci = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_mauri = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_menapii = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_montani = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_morini = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_musulamii = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_nervii = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_noricum = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_numidia = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_pannonia = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_petra = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_pontus = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_raeti = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_samaria = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_sardinia = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_sequani = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_sicambri = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_sunuici = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_syria = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_thrace = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_thrace_imp = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_tungri = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_tungri_imp = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_ubii = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_vangiones = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_vascones = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_aux_infantry_vindeli = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_bac_agema_hoplite = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bac_akontistai = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bac_axe_shield = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_bac_cata = {
		culture = "roman",
		class = "rich",
		count = 120
	},

	ai_bac_elephants = {
		culture = "other",
		class = "med",
		count = 48
	},

	ai_bac_elephants_elite = {
		culture = "other",
		class = "rich",
		count = 48
	},

	ai_bac_horse_archers = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_bac_indian_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_bac_indian_sword = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_bac_king_guard = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_bac_loncho = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_bac_noble_horse = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_bac_pantodapoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bac_phalanx_one = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_bac_phalanx_two = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_bac_thorakitai = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bac_thureophoroi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bac_toxotai = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_bac_two_axemen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_beja_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_beja_camel_warriors = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_beja_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_beja_greeks = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_beja_skirmishers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_beja_spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_beja_warriors = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_bith_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_bith_euzonoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bith_guard = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bith_heavy_cav = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_bith_light_cav = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_bith_peltasts = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bith_slingers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_bith_thureos = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bos_citizen_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_bos_citizen_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_bos_citizen_javelinmen = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bos_citizen_slingers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_bos_citizen_spearmen = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_bos_grescyth_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_bos_grescyth_cataphracts = {
		culture = "roman",
		class = "rich",
		count = 120
	},

	ai_bos_grescyth_cavalry = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_bos_grescyth_spearmen = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_bos_native_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_bos_native_infantry = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_bos_romanised_infantry = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_bos_royal_cav = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_bos_scyth_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_bos_scyth_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_bos_scyth_horse_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_boudica = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_Brit_Archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_Brit_Chariots = {
		culture = "barbarian",
		class = "med",
		count = 60
	},

	ai_Brit_Chieftain = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Druidic_Fanatics = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_Brit_Druidic_Only = {
		culture = "barbarian",
		class = "poor",
		count = 25
	},

	ai_Brit_Early_Warband = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Heavy_Horse = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_Brit_Heroes = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Late_Warband = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Leader = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Levied_Spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Brit_Light_Horse = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_Brit_Militia_Spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Brit_Naked_Spearband = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Naked_Warriors = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_Brit_Spearband = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Sword_Band = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Brit_Wardogs = {
		culture = "barbarian",
		class = "poor",
		count = 50
	},

	ai_byzantium_picked_cav = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_byzantium_picked_men = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_cantabrian_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cantabrian_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cantabrian_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cantabrian_cavalry = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_cantabrian_cavalry_noble = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_cantabrian_devotio_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cantabrian_devotio_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cantabrian_guerillas = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cantabrian_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cantabrian_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cantabrian_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cantabrian_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cappa_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_cappa_heavy_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_cappa_levies = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_cappa_light_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_cappa_royal_guard = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_cappa_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_cappa_warriors = {
		culture = "other",
		class = "poor",
		count = 310
	},

	ai_carthage_african_veterans = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_carthage_citizen_cav_1 = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_carthage_citizen_spear_1 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_carthage_citizen_spear_2 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_carthage_hannibal_bodyguard = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_carthage_iberian_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_carthage_iberian_cavalry_1 = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_carthage_iberian_cavalry_2 = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_carthage_iberian_scutari = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_carthage_italian_infantry = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_carthage_libyan_cavalry_1 = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_carthage_libyan_cavalry_2 = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_carthage_libyan_javelinmen_1 = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_carthage_libyan_javelinmen_2 = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_carthage_libyan_spearmen_1 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_carthage_libyan_spearmen_2 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_carthage_libyan_spearmen_3 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_carthage_marine = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_carthage_punic_spearmen_1 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_carthage_punic_spearmen_2 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_carthage_punic_spearmen_3 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_carthage_sacred_band_1 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_carthage_sacred_band_2 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_carthage_sacred_cav_1 = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_carthage_sacred_cav_2 = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_carthage_war_elephants = {
		culture = "roman",
		class = "med",
		count = 48
	},

	ai_cauc_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_cauc_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_cauc_colchis_cavalry = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_cauc_colchis_hoplites = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_cauc_colchis_militia = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_cauc_heavy_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_cauc_hillmen = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_cauc_levies = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_cauc_light_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_cauc_raiders = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_cauc_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_cauc_warlord = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_celt_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_celt_bodyguard_cav = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celt_chief = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celt_dog_handlers = {
		culture = "barbarian",
		class = "poor",
		count = 50
	},

	ai_celt_fanatics = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celt_free_horse = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_celt_free_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celt_hunters = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_celt_leader = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celt_levied_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_celt_light_horse = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_celt_militia = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_celt_noble_horse_early = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celt_noble_horse_late = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celt_phalanx = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_celt_skirm = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celt_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_celt_vercingetorix = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celt_warband_early = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celt_warband_late = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celt_youths = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_cavalry_devotio = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celtiberian_arevaci_cavalry_light = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_celtiberian_arevaci_cavalry_noble = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celtiberian_arevaci_cavalry_noble_2 = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celtiberian_arevaci_devotio_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_devotio_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_celtiberian_arevaci_warrior_spearmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_warrior_spearmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_warrior_spearmen_3 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_warrior_swordsmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_warrior_swordsmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_arevaci_warrior_swordsmen_3 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_cavalry_devotio = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celtiberian_cavalry_light = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_celtiberian_cavalry_noble = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celtiberian_cavalry_noble_2 = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_celtiberian_devotio_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_devotio_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_warrior_spearmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_warrior_spearmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_warrior_swordsmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_celtiberian_warrior_swordsmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_cretan_citizen_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_cretan_citizen_euzonoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_cretan_citizen_thureophoroi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_dac_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_dac_chieftain = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_dac_falxmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_dac_heavy_cav = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_dac_heavy_spears = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_dac_horse_archers = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_dac_light_cav = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_dac_nobles = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_dac_skirms = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_dac_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_dac_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_dac_swordsmen = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_doric_picked_cav = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_doric_picked_men = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_east_steppe_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_east_steppe_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_east_steppe_lancers = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_east_steppe_mounted_nobles = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_east_steppe_mounted_tribesmen = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_east_steppe_mounted_warband = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_east_steppe_tribesmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_east_steppe_warlord = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_east_steppe_warriors = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_epirus_agema_lancer = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_epirus_agema_shield = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_epirus_ambraciot_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_epirus_ambraciot_cavalry_shield = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_epirus_ambraciot_pikemen = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_epirus_chaonian_guard = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_epirus_chaonian_pikemen = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_epirus_citizen_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_epirus_molossian_pikemen = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_epirus_picked_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_epirus_thespotian_pikemen = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_Gaetuli_Cavalry_1 = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Gaetuli_Cavalry_2 = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Gaetuli_Javelinmen_1 = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Gaetuli_Noble_Cav_1 = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_Gaetuli_Warriors_1 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Gaetuli_Warriors_2 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_gal_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_gal_bodyguard_cav = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_gal_chief = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gal_dog_handlers = {
		culture = "barbarian",
		class = "poor",
		count = 50
	},

	ai_gal_fanatics = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gal_free_horse = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_gal_free_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gal_hunters = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_gal_leader = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gal_levied_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_gal_light_horse = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_gal_militia = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_gal_noble_horse_early = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_gal_noble_horse_late = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_gal_phalanx = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_gal_skirm = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gal_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_gal_warband_early = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gal_warband_late = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gal_youths = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gallaeci_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gallaeci_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gallaeci_cavalry_devotio = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_gallaeci_cavalry_noble = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_gallaeci_devotio_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gallaeci_devotio_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gallaeci_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gallaeci_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gallaeci_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gallaeci_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_gallaeci_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_gallaeci_tribesmen = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Garama_Bodyguard_1 = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Garama_Bowmen_1 = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Garama_Cavalry_1 = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Garama_Chariots_1 = {
		culture = "other",
		class = "poor",
		count = 60
	},

	ai_Garama_Infantry_1 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Garama_Raiders_1 = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Garama_Slingers_1 = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Ger_Bowmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_Ger_Bowmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_Ger_Champions_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Champions_1_east = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Champions_1_south = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Chieftain_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Chieftain_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Chieftain_2_east = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Chieftain_2_south = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Clubmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Clubmen_1_east = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Clubmen_1_south = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Clubmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Clubmen_2_east = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Clubmen_2_south = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Clubmen_3 = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Clubmen_3_east = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Clubmen_3_south = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Heavy_Horse_1 = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_Ger_Heavy_Horse_1_east = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_Ger_Heavy_Horse_1_south = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_Ger_Light_Horse_1 = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_Ger_Light_Horse_1_east = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_Ger_Light_Horse_1_south = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_Ger_Nobles_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Nobles_1_east = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Nobles_1_south = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Nobles_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Nobles_2_east = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Nobles_2_south = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Nobles_3 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Nobles_3_east = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Nobles_3_south = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Painted_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_Ger_Slingers_1 = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_Ger_Spear_Levy_1 = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Tribesmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Tribesmen_1_east = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Tribesmen_1_south = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Tribesmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Tribesmen_2_east = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Tribesmen_2_south = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Tribesmen_3 = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Tribesmen_3_east = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Tribesmen_3_south = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_Ger_Youths_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_greek_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_greek_ballista = {
		culture = "roman",
		class = "med",
		count = 30
	},

	ai_greek_javelinmen = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_greek_oxybeles = {
		culture = "roman",
		class = "slave",
		count = 30
	},

	ai_greek_slingers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_iberian_bastetani_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_bastetani_cavalry_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_bastetani_cavalry_scutari = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_iberian_bastetani_scutari_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_bastetani_scutari_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cavalry_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_cavalry_devotio = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_cavalry_scutari = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_cessetani_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_cavalry_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_cessetani_cavalry_devotio = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_iberian_cessetani_cavalry_scutari = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_iberian_cessetani_devotio_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_devotio_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_nobles_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_nobles_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_scutari_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_scutari_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_scutari_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_cessetani_scutari_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_devotio_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_devotio_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_cavalry_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_edetani_cavalry_devotio = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_edetani_cavalry_scutari = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_edetani_devotio_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_devotio_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_scutari_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_scutari_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_scutari_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_edetani_scutari_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_scutari_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_scutari_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_scutari_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_scutari_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_caetrati_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_caetrati_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_cavalry_caetrati = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_turdetani_cavalry_devotio = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_turdetani_cavalry_scutari = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_iberian_turdetani_devotio_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_devotio_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_nobles_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_nobles_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_scutari_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_scutari_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_scutari_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_iberian_turdetani_scutari_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_illy_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_illy_axemen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_illy_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_illy_light_horse = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_illy_nobles = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_illy_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_illy_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_imperial_roman_leg_iF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_iF_a = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_iF_m = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_iiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_iiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_iiiF_a = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_iiiF_c = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_iiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_iiiN_a = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_iiiN_c = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_iiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_iN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_iN_a = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_iN_m = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_ivF_f = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_ivF_s = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_ivN_f = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_ivN_s = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_vF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_vF_m = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_viF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_viF_c = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_viiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_viiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_viiiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_viiiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_viiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_viiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_viN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_viN_c = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_vN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_vN_m = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xF_f = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xiiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xiiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xivF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xivN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xN_f = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xvF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xviF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xviiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xviiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xviiiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xviiiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xviiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xviiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xviN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xvN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xxF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xxiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xxiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xxiiF_p = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_imperial_roman_leg_xxiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xxiiN_p = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xxiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_imperial_roman_leg_xxN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_kushite_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_kushite_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_kushite_elephants = {
		culture = "other",
		class = "poor",
		count = 24
	},

	ai_kushite_elephants_tower = {
		culture = "other",
		class = "rich",
		count = 48
	},

	ai_kushite_levied_spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_kushite_light_cavalry = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_kushite_longbows = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_kushite_militia_spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_kushite_nobles = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_kushite_royal_cavalry = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_kushite_royal_guard = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_kushite_swordsmen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_kushite_warlord = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_kushite_warriors = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_lig_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_lig_chieftain = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lig_heavy_horse = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_lig_levy_freemen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_lig_light_horse = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_lig_noble_warband = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lig_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_lig_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_lig_swordsmen = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_ligurian_light_infantry = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_lusitani_caetrati_light = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_caetrati_spear_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_caetrati_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_caetrati_sword_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_caetrati_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_cavalry_devotio = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_lusitani_cavalry_noble = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_lusitani_devotio_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_devotio_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_mountain_tribesmen_1 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_mountain_tribesmen_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_nobles_spear = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_nobles_spear_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_nobles_sword = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_nobles_sword_2 = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_lusitani_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_lustiani_cavalry_light = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_macedonian_agema = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_macedonian_agema_late = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_macedonian_agema_sword = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_macedonian_cavalry = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_macedonian_pikemen_bronze = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_macedonian_pikemen_white = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_macedonian_royal_cavalry = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_macedonian_royal_peltast = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_macedonian_royal_peltast_sword = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_macedonian_sacred_cavalry = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_macedonian_sacred_cavalry_lancers = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_marian_roman_leg_iF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_iiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_iiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_iiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_iiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_iN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_ivF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_ivN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_vF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_viF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_viiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_viiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_viiiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_viiiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_viiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_viiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_viN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_vN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_xF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_xiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_xiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_xiiiF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_xiiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_xiiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_xiN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_xivF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_xivN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_xN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_marian_roman_leg_xvF = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_marian_roman_leg_xvN = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_massalia_cavalry = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_massalia_militia = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_massalia_militia_late = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_massalia_picked = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_massinissa = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_Mauri_Archers_1 = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Mauri_Cavalry_1 = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Mauri_Javelinmen_1 = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Mauri_Noble_Cav_1 = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_Mauri_Spearmen_1 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_med_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_med_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_med_levy_spears = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_med_light_horse = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_med_noble_cavalry = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_med_nobles = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_med_skirms = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_med_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_merc_aethiopian_spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_merc_aetolian_thureophoroi = {
		culture = "roman",
		class = "poor",
		count = 240
	},

	ai_merc_african_elephants = {
		culture = "other",
		class = "poor",
		count = 24
	},

	ai_merc_african_warriors = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_merc_arab_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_merc_arab_camel_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_arab_camel_spears = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_arab_urban_camels = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_arab_urban_mounted = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_merc_arab_urban_nobles = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_merc_arab_urban_swords = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_merc_arab_warriors = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_merc_bactrian_hillmen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_merc_bactrian_horse_archers = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_merc_balearic = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_merc_bastarnae = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_merc_beja_camels = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_beja_spearmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_merc_briton_cav = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_merc_briton_dogs = {
		culture = "barbarian",
		class = "poor",
		count = 200
	},

	ai_merc_briton_warband = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_merc_cadusii_hillmen = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_merc_cappa_cavalry = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_merc_celtiberian_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_merc_celtiberian_scutari = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_merc_celtiberian_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_merc_celtic_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_merc_celtic_warband = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_merc_cretan_arch = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_merc_cretan_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_merc_cretan_inf = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_merc_dacian_falxmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_merc_dacian_swordsmen = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_merc_gaetuli_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_gaetuli_warriors = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_merc_galatian_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_merc_galatian_warband = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_merc_germanic_hunters = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_merc_germanic_spears = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_merc_germanic_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_merc_getic_horse_archers = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_merc_illyrian_peltast = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_merc_illyrian_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_merc_indian_longbows = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_merc_indian_swordsmen = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_merc_kushite_longbows = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_merc_kushite_warriors = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_merc_libyan_bowmen = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_merc_libyan_chariots = {
		culture = "other",
		class = "poor",
		count = 60
	},

	ai_merc_libyan_skirms = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_merc_lig_warband = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_merc_lig_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_merc_lucanian_spearmen = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_merc_mauri_light_foot = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_merc_mauri_light_horse = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_north_african_elephants = {
		culture = "other",
		class = "poor",
		count = 24
	},

	ai_merc_numidian_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_numidian_skirms = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_merc_parthian_light_horse = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_rhodes_sling = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_merc_saka_axemen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_merc_saka_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_merc_saka_horse_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_samnite_spearmen = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_merc_sarmatian_cata = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_merc_sarmatian_horse_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_sarmatian_horse_skirms = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_scythian_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_merc_scythian_horse_archers = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_merc_tarantine_cav = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_merc_thorakitai_early = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_merc_thorakitai_late = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_merc_thracian_light_cav = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_merc_thracian_peltasts = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_merc_thureophoroi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_neocretan_euzonoi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_Numidian_Archers_1 = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Numidian_Cavalry_1 = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Numidian_Cavalry_2 = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Numidian_Infantry_1 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Numidian_Infantry_2 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Numidian_Infantry_3 = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_Numidian_Javelinmen_1 = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Numidian_Noble_Cav_1 = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_Numidian_Noble_Cav_2 = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_Numidian_Picked_Infantry_1 = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_Numidian_Slingers_1 = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_Numidian_War_Elephants_1 = {
		culture = "other",
		class = "poor",
		count = 24
	},

	ai_oikeis_archers = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_parthian_foot_archers_dei = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_parthian_horse_archers_dei = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_parthian_noble_cataphracts_early_dei = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_parthian_noble_cataphracts_late_dei = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_parthian_noble_horse_archers_dei = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_parthian_royal_cataphracts_dei = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_perg_agema = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_perg_citizen_militia = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_perg_citizen_pikes = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_perg_royal_guard = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_pont_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_pont_light_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_pont_pikes = {
		culture = "other",
		class = "poor",
		count = 256
	},

	ai_pont_reformed_spears = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_pont_romanised = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_pont_royal_swords = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_pont_slave_spears = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_pont_slingers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_ptol_armoured_elephant = {
		culture = "roman",
		class = "rich",
		count = 48
	},

	ai_ptol_chariots = {
		culture = "roman",
		class = "rich",
		count = 40
	},

	ai_ptol_galatian_settler_inf = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_ptol_jewish_settler_inf = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_ptol_libyan_pikemen = {
		culture = "other",
		class = "poor",
		count = 256
	},

	ai_ptol_macedonian_cav = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_ptol_military_settler_cav = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_ptol_military_settler_inf = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_ptol_mysian_cav = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_ptol_native_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_ptol_native_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_ptol_native_libyan_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_ptol_native_light_inf = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_ptol_native_pikemen = {
		culture = "other",
		class = "poor",
		count = 256
	},

	ai_ptol_native_spear_bearers = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_ptol_paid_cav = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_ptol_paid_inf = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_ptol_peltasts = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_ptol_peltasts_roman = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_ptol_persian_cav = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_ptol_picked_machimoi = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_ptol_picked_machimoi_roman = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_ptol_royal_guard = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_ptol_royal_guard_cav = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_ptol_spear_bearers = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_ptol_sword_bearers = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_ptol_thessalian_cav = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_ptol_thracian_cav = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_ptol_thracian_settler_inf = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_rhodes_picked_men = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_rhodes_sling = {
		culture = "roman",
		class = "slave",
		count = 180
	},

	ai_rom_antesignani = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_rom_augustan_praetorians = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_ballista = {
		culture = "roman",
		class = "med",
		count = 30
	},

	ai_rom_ballista_bastion = {
		culture = "roman",
		class = "med",
		count = 30
	},

	ai_rom_cheiroballistra = {
		culture = "roman",
		class = "poor",
		count = 30
	},

	ai_rom_eastern_evocata = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_equites_legionis = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_rom_evocati_cohort = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_giant_ballista = {
		culture = "roman",
		class = "rich",
		count = 30
	},

	ai_rom_imperial_praetorians = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_large_onager = {
		culture = "roman",
		class = "rich",
		count = 30
	},

	ai_rom_legatus = {
		culture = "roman",
		class = "slave",
		count = 25
	},

	ai_rom_legionary_cav = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_rom_onager = {
		culture = "roman",
		class = "med",
		count = 30
	},

	ai_rom_plebs = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_polybolos = {
		culture = "roman",
		class = "slave",
		count = 30
	},

	ai_rom_polybolos_bastion = {
		culture = "roman",
		class = "slave",
		count = 30
	},

	ai_rom_praetorian_cav = {
		culture = "roman",
		class = "rich",
		count = 120
	},

	ai_rom_scorpio = {
		culture = "roman",
		class = "slave",
		count = 30
	},

	ai_rom_scorpio_bastion = {
		culture = "roman",
		class = "slave",
		count = 30
	},

	ai_rom_townwatch1 = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_townwatch2 = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_townwatch3 = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_urban_cohort = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_vet_legionaries = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_vexillatio_evocata_germanica = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_rom_vigiles = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_bodyguard_cavalry_early = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_bodyguard_cavalry_late = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_roman_bodyguard_infantry_early = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_bodyguard_infantry_late = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_equites_10 = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_equites_20 = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_roman_hastati_10 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_hastati_20 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_hastati_30 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_hastati_40 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_leves = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_principes_10 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_principes_20 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_principes_30 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_principes_40 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_scipio_bodyguard = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_roman_socii_10_etruscan = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_10_latin = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_10_lucanian = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_10_samnite = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_20_etruscan = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_20_latin = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_20_lucanian = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_20_samnite = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_25 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_25_etruscan = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_25_latin = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_25_lucanian = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_25_samnite = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_30 = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_equites_campani = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_etruscan = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_extraordinarii = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_extraordinarii_campani = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_extraordinarii_etruscan = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_extraordinarii_late = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_roman_socii_equites_extraordinarii_lucanian = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_extraordinarii_samnite = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_late = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_roman_socii_equites_latin = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_latin_late = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_roman_socii_equites_lucanian = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_samnite = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_equites_tarantine = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_roman_socii_extraordinarii_10 = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_socii_extraordinarii_10_etruscan = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_socii_extraordinarii_10_lucanian = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_socii_extraordinarii_10_samnite = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_socii_extraordinarii_20 = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_socii_extraordinarii_20_etruscan = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_socii_extraordinarii_20_lucanian = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_socii_extraordinarii_20_samnite = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_socii_leves_etruscan = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_leves_latin = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_leves_lucanian = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_leves_samnite = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_velites = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_socii_velites_latin = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_roman_triarii_10 = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_triarii_20 = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_triarii_30 = {
		culture = "roman",
		class = "slave",
		count = 300
	},

	ai_roman_velites = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_rome_augustus = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_rome_caesar = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_rome_pompey = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_sarm_steppe_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_sarm_steppe_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_sarm_steppe_lancers = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_sarm_steppe_mounted_nobles = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_sarm_steppe_mounted_tribesmen = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_sarm_steppe_mounted_warband = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_sarm_steppe_skirms = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_sarm_steppe_tribesmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_sarm_steppe_warlord = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_sarm_steppe_warriors = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_sele_agema = {
		culture = "roman",
		class = "rich",
		count = 120
	},

	ai_sele_antiochus = {
		culture = "roman",
		class = "rich",
		count = 120
	},

	ai_sele_argyraspides = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_sele_chalkaspides = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_sele_elephant = {
		culture = "roman",
		class = "med",
		count = 48
	},

	ai_sele_elite_elephant = {
		culture = "roman",
		class = "rich",
		count = 48
	},

	ai_sele_galatian = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_sele_galatian_two = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_sele_hetairoi = {
		culture = "roman",
		class = "rich",
		count = 120
	},

	ai_sele_hypaspists = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_sele_kataphraktoi = {
		culture = "roman",
		class = "rich",
		count = 120
	},

	ai_sele_katoikoi_one = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_sele_katoikoi_two = {
		culture = "roman",
		class = "slave",
		count = 256
	},

	ai_sele_lonchoporoi = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_sele_romanized = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_sele_tarentine_cav = {
		culture = "roman",
		class = "poor",
		count = 120
	},

	ai_sele_thureophoroi = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_sele_xystophoroi = {
		culture = "roman",
		class = "slave",
		count = 120
	},

	ai_southern_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_southern_levies = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_southern_warriors = {
		culture = "other",
		class = "poor",
		count = 240
	},

	ai_thrac_archers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_thrac_heavy_horse = {
		culture = "barbarian",
		class = "med",
		count = 120
	},

	ai_thrac_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_thrac_light_horse = {
		culture = "barbarian",
		class = "poor",
		count = 120
	},

	ai_thrac_nobles = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_thrac_nobles_shock = {
		culture = "barbarian",
		class = "poor",
		count = 240
	},

	ai_thrac_peltasts = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_thrac_royal_cav = {
		culture = "roman",
		class = "med",
		count = 120
	},

	ai_thrac_royal_guard = {
		culture = "roman",
		class = "slave",
		count = 240
	},

	ai_thrac_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 180
	},

	ai_thrac_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 300
	},

	ai_west_steppe_archers = {
		culture = "other",
		class = "poor",
		count = 180
	},

	ai_west_steppe_cataphracts = {
		culture = "other",
		class = "rich",
		count = 120
	},

	ai_west_steppe_lancers = {
		culture = "other",
		class = "med",
		count = 120
	},

	ai_west_steppe_mounted_nobles = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_west_steppe_mounted_tribesmen = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_west_steppe_mounted_warband = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_west_steppe_tribesmen = {
		culture = "other",
		class = "poor",
		count = 300
	},

	ai_west_steppe_warlord = {
		culture = "other",
		class = "poor",
		count = 120
	},

	ai_west_steppe_warriors = {
		culture = "other",
		class = "poor",
		count = 240
	},

	att_rom_civilian_farmers = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_rom_civilian_townsfolk = {
		culture = "other",
		class = "poor",
		count = 40
	},
}